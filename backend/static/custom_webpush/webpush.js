/* Modificado por Alejandro Díaz para adaptar a las necesidades.
    Se ha respetado la creacion y destruccion de subscripciones, pero se ha modificado la gestion de los estados.
    Incluye:
        - Refactorizacion del codigo para gestionar los estados mediante handlers
 */

// Extraido de: https://github.com/safwanrahman/django-webpush/blob/master/webpush/static/webpush/webpush.js

/* ORIGINAL COPYRIGHT
/ Copyright © 2018 Safwan Rahman
/ This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
/ This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
/ You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
/ Based On https://github.com/chrisdavidmills/push-api-demo/blob/283df97baf49a9e67705ed08354238b83ba7e9d3/main.js
 */

let registration, notificationsEnabled;

let notificationButton;

function closeWall(wall) {
    document.getElementById(wall).classList.add('notif-wall--hidden');
    setWallShown(wall);
}

function checkWallShown(wall) {
    return sessionStorage.getItem(wall) == '1'
}

function setWallShown(wall) {
    sessionStorage.setItem(wall, '1')
}

function showWall(wall) {
    document.getElementById(wall).classList.remove('notif-wall--hidden');
}

function onInvalidBrowser() {
    // El navegador no es apropiado
    if (!checkWallShown('invalid-browser-wall')) {
         showWall('invalid-browser-wall');
    }
    document.getElementById('invalid-browser-msg').style.display = ''
    notificationButton.remove()
}

function onNotificationsBlocked() {
    // El navegador no es apropiado
    if (!checkWallShown('notification-blocked-wall')) {
         showWall('notification-blocked-wall');
    }
    document.getElementById('notification-blocked-msg').style.display = ''
    notificationButton.remove()
    return;
}

function setNotificationsEnabled() {
    notificationsEnabled = true;
    notificationButton.disabled = false;
    notificationButton.classList.remove('button-notification--enable');
    notificationButton.classList.add('button-notification--disable');
}

function setNotificationsDisabled() {
    notificationsEnabled = false;
    notificationButton.disabled = false;
    notificationButton.classList.remove('button-notification--disable');
    notificationButton.classList.add('button-notification--enable');
    if (!checkWallShown('enable-notif-wall')) {
         showWall('enable-notif-wall');
    }
}

function postSubscribeObj(statusType, subscription) {
  // Send the information to the server with fetch API.
  // the type of the request, the name of the user subscribing,
  // and the push subscription endpoint + key the server needs
  // to send push messages

  const browser = navigator.userAgent.match(/(firefox|msie|chrome|safari|trident)/ig)[0].toLowerCase(),
    data = {  status_type: statusType,
              subscription: subscription.toJSON(),
              browser: browser,
           };

  fetch('/webpush/save_information', {
    method: 'post',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data),
    credentials: 'include'
  }).then(
      function(response) {
        // Check the information is saved successfully into server
        if ((response.status == 201) && (statusType == 'subscribe')) {
          setNotificationsEnabled()
        }

        // Check if the information is deleted from server
        if ((response.status == 202) && (statusType == 'unsubscribe')) {
          // Get the Subscription
          getSubscription(registration)
            .then(
              function(subscription) {
                // Remove the subscription
                subscription.unsubscribe()
                .then(
                  function(successful) {
                    setNotificationsDisabled()
                  }
                )
              }
            )
            .catch(
              function(error) {
                setNotificationsEnabled();
              }
            );
        }
      }
    )
}

function initialize(reg) {
    /* Inicializa el estado */
    if (!(reg.showNotification)) {
        // No se pueden mostrar notificaciones
        return onInvalidBrowser();
    }

    if (!('PushManager' in window)) {
        /* Si no se pueden hacer notificaciones PUSH se notifica */
        return onInvalidBrowser();
    }
    // We need to get subscription state for push notifications and send the information to server
    reg.pushManager.getSubscription().then(
        function(subscription) {
            if (subscription){
                postSubscribeObj('subscribe', subscription,
                    function(response) {
                        // Check the information is saved successfully into server
                        if (response.status === 201) {
                            setNotificationsEnabled()
                        } else {
                            setNotificationsDisabled()
                        }
                    });
            } else {
                setNotificationsDisabled()
            }
        });
}


function subscribe(reg) {
    // Get the Subscription or register one
    reg.pushManager.getSubscription().then(
        function(subscription) {
            var metaObj, applicationServerKey, options;
            // Check if Subscription is available
            if (subscription) {
                return subscription;
            }

            metaObj = document.querySelector('meta[name="django-webpush-vapid-key"]');
            applicationServerKey = metaObj.content;
            options = {
                userVisibleOnly: true
            };
            if (applicationServerKey){
                options.applicationServerKey = urlB64ToUint8Array(applicationServerKey)
            }
            // If not, register one
            reg.pushManager.subscribe(options)
                .then(
                    function(subscription) {
                        postSubscribeObj('subscribe', subscription,
                            function(response) {
                                // Check the information is saved successfully into server
                                if (response.status === 201) {
                                    setNotificationsEnabled()
                                }
                            });
                    })
                .catch(
                    function() {
                        console.log('Subscription error.', arguments)
                    })
        }
    );
}

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (var i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

function getSubscription(reg) {
    return reg.pushManager.getSubscription().then(
        function(subscription) {
          var metaObj, applicationServerKey, options;
          // Check if Subscription is available
          if (subscription) {
            return subscription;
          }

          metaObj = document.querySelector('meta[name="django-webpush-vapid-key"]');
          applicationServerKey = metaObj.content;
          options = {
              userVisibleOnly: true
          };
          if (applicationServerKey){
              options.applicationServerKey = urlB64ToUint8Array(applicationServerKey)
          }
          // If not, register one
          return registration.pushManager.subscribe(options)
        }
      )
}

function unsubscribe() {
  // Get the Subscription to unregister
  registration.pushManager.getSubscription()
    .then(
      function(subscription) {

        // Check we have a subscription to unsubscribe
        if (!subscription) {
          return setNotificationsDisabled();
        }
        postSubscribeObj('unsubscribe', subscription);
      }
    )
}

window.addEventListener('load',
    function() {
        notificationButton = document.getElementById('webpush-notification-button');

        notificationButton.addEventListener('click',
            function() {
                notificationButton.disabled = true;
                return notificationsEnabled ? unsubscribe(registration):subscribe(registration);
            }
        );

        // Do everything if the Browser Supports Service Worker
        if ('serviceWorker' in navigator) {
            const serviceWorker = document.querySelector('meta[name="service-worker-js"]').content;
            navigator.serviceWorker.register(serviceWorker).then(
                function(reg) {
                    registration = reg;
                    initialize(reg);
                });
        } else {
            return onInvalidBrowser()
        }
    }
);