from datetime import timedelta, datetime
from time import sleep

import pytz
from django.core.management import BaseCommand
from django.utils import timezone

from device.constants import HEARTBEAT_MAX_SECONDS, NOTIFICATION_TYPE_OFF
from device.models import Device
from device.utils import send_notification_for_device
from mqtt.publish import mqtt_publish_conf


class Command(BaseCommand):
    """
    Este comando comprobara cada 5 segundos los dispositivos encendidos, y si hace mas de HEARTBEAT_MAX_SECONDS
    que no se ha recibido una señal, se consideraran apagados y se enviara una notificacion.
    Al mismo tiempo, comprueba los dispositivos que haya que activar o desactivar el modo noche
    """

    def handle(self, *args, **options):
        print('Check Devices On - Alejandro Díaz')
        try:
            last_time = datetime.now(pytz.timezone('Europe/Madrid')) - timedelta(seconds=10)
            while True:
                start_time = datetime.now(pytz.timezone('Europe/Madrid'))

                # Check devices off
                devices = Device.objects.filter(
                    is_device_on=True,
                    last_seen_at__lte=timezone.now() - timedelta(seconds=HEARTBEAT_MAX_SECONDS)
                )

                dev_list = list(devices)

                devices.update(
                    is_device_on=False
                )

                for device in dev_list:
                    send_notification_for_device(device, NOTIFICATION_TYPE_OFF)

                if len(dev_list) > 0:
                    print(f'{len(dev_list)} devices turned off')

                # Search Devices to turn night mode on.
                # To do that, we search those whose time is in range last check and now
                devices = Device.objects.filter(
                    is_night_mode_enabled=False,
                    night_mode_start__gte=last_time.time(),
                    night_mode_start__lte=start_time.time()
                )
                updated = list(devices)
                devices.update(
                    is_night_mode_enabled=True
                )

                for dev in updated:
                    # Publish new conf
                    dev.refresh_from_db()
                    mqtt_publish_conf(dev)

                # Search Devices to turn night mode off.
                # To do that, we search those whose time is in range last check and now
                devices = Device.objects.filter(
                    is_night_mode_enabled=True,
                    night_mode_end__gte=last_time.time(),
                    night_mode_end__lte=start_time.time()
                )
                updated = list(devices)
                devices.update(
                    is_night_mode_enabled=False
                )

                for dev in updated:
                    # Publish new conf
                    dev.refresh_from_db()
                    mqtt_publish_conf(dev)
                last_time = start_time
                sleep(5)
        except KeyboardInterrupt:
            print('\nBye!')
