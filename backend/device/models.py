from django.contrib.auth import get_user_model
from django.core import validators
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _, ugettext

from device.constants import NOTIFICATION_TYPE_UP, NOTIFICATION_TYPE_LEFT, NOTIFICATION_TYPE_DOWN, \
    NOTIFICATION_TYPE_RIGHT, NOTIFICATION_TYPE_UP_LEFT, NOTIFICATION_TYPE_UP_RIGHT, NOTIFICATION_TYPE_TEMP_HIGH, \
    NOTIFICATION_TYPE_TEMP_LOW, NOTIFICATION_TYPE_LIGHT, NOTIFICATION_TYPE_OFF
from device.managers import DeviceSyncCodeManager

User = get_user_model()


class DeviceBaseModel(models.Model):
    """
        Modelo Basico que incluye los campos para la fecha de creacion y la fecha de modificacion
    """
    created = models.DateTimeField(
        verbose_name=_('Created date'),
        auto_now_add=True,
    )
    last_modified = models.DateTimeField(
        verbose_name=_('Last Modified date'),
        auto_now=True,
    )

    class Meta:
        abstract = True


class Device(DeviceBaseModel):
    """
        Modelo del dispositivo
    """
    identifier = models.CharField(
        db_index=True,
        verbose_name=_('Device ID'),
        max_length=20,
        unique=True
    )
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=255,
        default='Asistente Digital'
    )
    owners = models.ManyToManyField(
        verbose_name=_('Owners'),
        related_name='devices',
        to=User,
        blank=True
    )
    joystick_up = models.CharField(
        verbose_name=_('Joystick Up'),
        max_length=20,
        default='',
        blank=True
    )
    joystick_left = models.CharField(
        verbose_name=_('Joystick left'),
        max_length=20,
        default='',
        blank=True
    )
    joystick_down = models.CharField(
        verbose_name=_('Joystick Down'),
        max_length=20,
        default='',
        blank=True
    )
    joystick_right = models.CharField(
        verbose_name=_('Joystick Right'),
        max_length=20,
        default='',
        blank=True
    )
    joystick_up_left = models.CharField(
        verbose_name=_('Joystick Up-Left'),
        max_length=20,
        default='',
        blank=True
    )
    joystick_up_right = models.CharField(
        verbose_name=_('Joystick Up-Right'),
        max_length=20,
        default='',
        blank=True
    )
    joystick_down = models.CharField(
        verbose_name=_('Joystick Down'),
        max_length=20,
        default='Cancelar',
    )
    min_temperature = models.PositiveSmallIntegerField(
        verbose_name=_('Min Temperature'),
        validators=[
            validators.MinValueValidator(1),
            validators.MaxValueValidator(40)
        ],
        null=True,
        blank=True
    )
    max_temperature = models.PositiveSmallIntegerField(
        verbose_name=_('Max Temperature'),
        validators=[
            validators.MinValueValidator(1),
            validators.MaxValueValidator(40)
        ],
        null=True,
        blank=True
    )
    is_night_mode_enabled = models.BooleanField(
        verbose_name=_('Night Mode Enabled'),
        default=False
    )
    is_device_on = models.BooleanField(
        verbose_name=_('Night Mode Enabled'),
        default=False
    )
    last_seen_at = models.DateTimeField(
        verbose_name=_('Last Seen At'),
        null=True,
        blank=True
    )
    wifi_ssid = models.CharField(
        verbose_name=_('Wi-Fi SSID'),
        max_length=32,
        blank=True,
        null=True
    )
    wifi_password = models.CharField(
        verbose_name=_('Wi-Fi Password'),
        max_length=63,
        blank=True,
        null=True
    )
    night_mode_start = models.TimeField(
        verbose_name=_('Night Mode Start At'),
        null=True,
        blank=True
    )
    night_mode_end = models.TimeField(
        verbose_name=_('Night Mode End At'),
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('name', 'created')
        verbose_name = _('Device')
        verbose_name_plural = _('Devices')

    def __str__(self):
        return f'{self.name} ({self.identifier})'

    def get_notification_message(self, notification_type):
        messages = {
            NOTIFICATION_TYPE_UP: self.joystick_up or '',
            NOTIFICATION_TYPE_LEFT: self.joystick_left or '',
            NOTIFICATION_TYPE_DOWN: self.joystick_down or '',
            NOTIFICATION_TYPE_RIGHT: self.joystick_right or '',
            NOTIFICATION_TYPE_UP_LEFT: self.joystick_up_left or '',
            NOTIFICATION_TYPE_UP_RIGHT: self.joystick_up_right or '',
            NOTIFICATION_TYPE_TEMP_HIGH: f'{ugettext("La temperatura es superior a")} {self.max_temperature} ºC',
            NOTIFICATION_TYPE_TEMP_LOW: f'{ugettext("La temperatura es inferior a")} {self.min_temperature} ºC',
            NOTIFICATION_TYPE_LIGHT: ugettext('Se ha encendido una luz'),
            NOTIFICATION_TYPE_OFF: ugettext('El dispositivo puede estar apagado')
        }
        return messages[notification_type]

    def set_device_on(self):
        self.is_device_on = True
        self.last_seen_at = timezone.now()
        self.save()


class DeviceSyncCode(DeviceBaseModel):
    """
        Modelo del codigo de sincronizacion
    """
    device = models.ForeignKey(
        verbose_name=_('Device'),
        to=Device,
        on_delete=models.CASCADE,
    )
    sync_code = models.CharField(
        db_index=True,
        verbose_name=_('Sync Code'),
        max_length=9,
        unique=True
    )
    used = models.BooleanField(
        verbose_name=_('Used'),
        default=False
    )
    used_by = models.ForeignKey(
        verbose_name=_('Used by'),
        to=User,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    objects = DeviceSyncCodeManager()

    class Meta:
        ordering = ('-created', )
        verbose_name = _('Device Sync Code')
        verbose_name_plural = _('Device Sync Codes')

    def __str__(self):
        return f'{self.sync_code} ({self.device}). Used: {self.used}'

    def use(self, user):
        """
        Utiliza un codigo de sincronizacion
        :param user: Usuario que utiliza el codigo
        :return: None
        """
        # Anyadimos el usuario a los duenyos del dispositivo
        self.device.owners.add(user)
        # Marcamos el codigo como usado e indicamos quien lo ha usado
        self.used = True
        self.used_by = user
        # Guardamos los cambios
        self.save()
