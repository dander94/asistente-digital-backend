from django.urls import path

from device.views import DeviceIndexView, DeviceSyncCodeView, DeviceUpdateView

urlpatterns = [
    path('edit/<pk>/', DeviceUpdateView.as_view(), name='update'),
    path('sync/', DeviceSyncCodeView.as_view(), name='sync'),
    path('', DeviceIndexView.as_view(), name='index'),
]