from webpush import send_user_notification


def send_notification_for_device(device, notification_type):
    """
    Metodo para enviar una notificacion de un dispostivio
    :param device:
    :param notification_type:
    :return:
    """
    try:
        message = device.get_notification_message(notification_type)
    except KeyError:
        return False
    payload = {
        'head': device.name,
        'body': message
    }
    for owner in device.owners.all():
        send_user_notification(user=owner, payload=payload, ttl=86400)
    return True