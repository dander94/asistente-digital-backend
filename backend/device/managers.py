import random
import string

from django.db.models import Manager


class DeviceSyncCodeManager(Manager):

    def get_or_create_for_device(self, device):
        """
        Recupera un codigo de sync no usado para un dispositivo o lo crea.
        :param device: Dispositivo que se asociara al codigo
        :return: DeviceSyncCode
        """
        qs = self.get_queryset()

        try:
            # Intentamos devolver un codigo que ya exista
            return qs.get(
                device=device,
                used=False
            )
        except self.model.DoesNotExist:
            pass
        # Excluimos algunas letras y numeros que son dificiles de distinguir
        excluded_letters = ['B', 'I', 'O', 'S']
        excluded_numbers = ['8', '1', 'O', '5']

        # Combinamos todos los caracteres posibles
        chars = [
            c for c in (string.ascii_uppercase + string.digits)
            if c not in excluded_letters and c not in excluded_numbers
        ]
        exists = True
        # Generamos un codigo de forma aleatoria y comprobamos si ya existe
        while exists:
            code = ''.join(random.choices(chars, k=9))
            exists = qs.filter(
                sync_code=code,
                used=False
            ).exists()
        # Una vez se ha generado un codigo unico, se crea y se devuelve
        return qs.create(
            device=device,
            sync_code=code
        )
