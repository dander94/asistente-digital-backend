from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.utils.translation import ugettext
from django.views.generic import TemplateView, FormView, UpdateView

from base.mixins import LoginRequiredMixin
from device.forms import DeviceSyncCodeForm, DeviceForm
from device.models import DeviceSyncCode
from mqtt.publish import mqtt_publish_conf


class DeviceAlertMixin:

    def add_alert(self, msg, is_error=False):
        self.request.session['alert'] = {
            'msg': msg,
            'is_error': is_error
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['alert'] = self.request.session.pop('alert', None)
        return context


class DeviceIndexView(DeviceAlertMixin, LoginRequiredMixin, TemplateView):
    """
        Vista principal del dashboard. Requiere login
    """
    template_name = 'device/index.html'

    def get_context_data(self, **kwargs):
        # Recuperamos los dispositivos del usuario y los anyadimos al contexto de la vista
        context = super().get_context_data(**kwargs)
        context['devices'] = self.request.user.devices.all()
        return context


class DeviceSyncCodeView(DeviceAlertMixin, LoginRequiredMixin, FormView):
    """
        Vista para utilizar un codigo de sincronizacion y vincular un dispositivo
    """
    form_class = DeviceSyncCodeForm
    template_name = 'device/sync.html'

    def form_valid(self, form):
        # Recuperamos el codigo de la base de datos
        sync_code = DeviceSyncCode.objects.get(
            sync_code=form.cleaned_data['sync_code']
        )
        # Lo utilizamos
        sync_code.use(self.request.user)
        self.add_alert(ugettext('Su dispositivo se ha sincronizado correctamente'))
        return HttpResponseRedirect(reverse('device:update', args=(sync_code.device.pk, )))

    def form_invalid(self, form):
        self.add_alert(ugettext('El código no es válido'), is_error=True)
        return super().form_invalid(form)


class DeviceUpdateView(DeviceAlertMixin, LoginRequiredMixin, UpdateView):
    """
        Vista para modificar un dispositivo y configurarlo
    """
    form_class = DeviceForm
    success_url = reverse_lazy('device:index')
    template_name = 'device/update.html'

    def get_queryset(self):
        # Recuperamos solo los dispostivos del usuario
        return self.request.user.devices.all()

    def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            context['original'] = self.get_object()
            return context

    def form_valid(self, form):
        # Publicamos la nueva configuracion
        ret = super().form_valid(form)
        mqtt_publish_conf(self.get_object())
        self.add_alert(ugettext('Sus cambios se han guardado correctamente'))
        return ret

    def form_invalid(self, form):
        self.add_alert(ugettext('La configuración es incorrecta. Por favor, corrija los errores'), is_error=True)
        return super().form_invalid(form)
