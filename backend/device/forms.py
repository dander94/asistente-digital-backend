from django import forms
from django.utils.translation import ugettext_lazy as _, ugettext
from device.models import DeviceSyncCode, Device


class DeviceSyncCodeForm(forms.Form):
    """
        Formulario para la vista de sincronizacion
    """
    sync_code = forms.CharField(
        max_length=9,
        required=True
    )

    class Meta:
        fields = ('sync_code', )

    def clean_sync_code(self):
        value = self.cleaned_data['sync_code']
        # comprobamos si existe un codigo que no esté utilizado y que el valor sea el introducido
        exists = DeviceSyncCode.objects.filter(
            sync_code=value,
            used=False
        ).exists()
        if not exists:
            raise forms.ValidationError(_('El código no es válido'))
        return value


class DeviceForm(forms.ModelForm):
    """
        Formulario para la vista de configuracion de un dispositivo
    """
    class Meta:
        fields = (
            'name', 'joystick_up', 'joystick_left', 'joystick_down', 'joystick_right', 'joystick_up_left',
            'joystick_up_right', 'joystick_down', 'min_temperature', 'max_temperature', 'is_night_mode_enabled',
            'wifi_ssid', 'wifi_password', 'night_mode_start', 'night_mode_end'
        )
        model = Device

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['wifi_password'].widget.input_type = 'password'
        self.fields['night_mode_start'].widget.input_type = 'time'
        self.fields['night_mode_end'].widget.input_type = 'time'

    def clean(self):
        errors = {}
        if self.cleaned_data.get('min_temperature') is not None and self.cleaned_data.get('max_temperature') \
                is not None and self.cleaned_data['min_temperature'] > self.cleaned_data['max_temperature']:
            errors['max_temperature'] = [forms.ValidationError(
                _("Min temperature can't be higher than max temperature"), code='invalid_temp'
            )]

        wifi_ssid = self.cleaned_data.get('wifi_ssid')
        wifi_pass = self.cleaned_data.get('wifi_password')

        if (not wifi_ssid and wifi_pass) or (wifi_ssid and not wifi_pass):
            errors['wifi_password'] = [forms.ValidationError(
                _('Wi-Fi Configuration must be either full or blank'), code='invalid_wifi'
            )]
        if errors:
            raise forms.ValidationError(errors)
        return self.cleaned_data

