from django.urls import path

from user.views import LogoutView, SignupView, LoginView

urlpatterns = [
    path('logout/', LogoutView.as_view(), name='logout'),
    path('signup/', SignupView.as_view(), name='signup'),
    path('login/', LoginView.as_view(), name='login')
]