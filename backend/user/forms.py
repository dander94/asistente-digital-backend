from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import ugettext_lazy as _


User = get_user_model()  # El modelo de usuario nunca se debe importar directamente, siempre lo debe proveer django.


class SignUpForm(UserCreationForm):
    """
        Formulario para registrar un usuario. Modificacion del original de Django
    """
    email = forms.EmailField(
        max_length=255
    )
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        min_length=6
    )

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2', )


class LoginForm(forms.Form):
    """
        Formulario para autentificar a un usuario
    """
    email = forms.EmailField(
        max_length=255
    )
    password = forms.CharField(
        max_length=128,
        widget=forms.PasswordInput()
    )

    def clean(self):
        # Recuperamos los datos
        cleaned_data = super().clean()
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        # Comprobamos si existe un usuario que cumpla con los datos proporcionados
        user = authenticate(username=email, password=password)
        if user is None:
            # La autenticacion ha fallado
            raise forms.ValidationError(
                _('Invalid email or password')
            )
        return cleaned_data
