from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy, reverse
from django.views.generic import FormView, TemplateView

from user.forms import SignUpForm, LoginForm


class RedirectAuthenticatedMixin:

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('device:index'))
        return super().dispatch(request, *args, **kwargs)


class SignupView(RedirectAuthenticatedMixin, FormView):
    """
        Vista de Signup
    """
    form_class = SignUpForm
    success_url = reverse_lazy('device:index')
    template_name = 'user/signup.html'

    def form_valid(self, form):
        # Creamos el usuario
        form.save()
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password1')
        # Obtenemos el usuario creado y listo para la autenticacion
        user = authenticate(username=email, password=password)
        # Autenticamos al usuario
        login(self.request, user)
        return super().form_valid(form)


class LoginView(RedirectAuthenticatedMixin, FormView):
    """
        Vista de Login
    """
    form_class = LoginForm
    success_url = reverse_lazy('device:index')
    template_name = 'user/login.html'

    def form_valid(self, form):
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        # Obtenemos el usuario creado y listo para la autenticacion
        user = authenticate(username=email, password=password)
        # Autenticamos al usuario
        login(self.request, user)
        return super().form_valid(form)


class LogoutView(TemplateView):
    """
        Vista de logout. Redirigirá al login. Se usa una plantilla ya que se debe borrar el sessionStorage
    """
    template_name = 'user/logout.html'

    def dispatch(self, request, *args, **kwargs):
        logout(self.request)
        return super().dispatch(request, *args, **kwargs)
