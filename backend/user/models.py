from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from user.managers import UserManager


class User(AbstractUser):
    """
    Model de usu
    """
    username = None
    email = models.EmailField(
        verbose_name=_('email address'),
        unique=True
    )

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
