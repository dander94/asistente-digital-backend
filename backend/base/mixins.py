from django.contrib.auth.mixins import LoginRequiredMixin as DjangoLoginRequiredMixin

from django.urls import reverse_lazy


class LoginRequiredMixin(DjangoLoginRequiredMixin):
    """
        Sobreescribimos el mixin de login required de Django para que redirija a nuestra vista de login
    """
    login_url = reverse_lazy('user:login')
