def log_packet(mqtt_packet):
    """
    Metodo para generar una string con el contenido de un paquete
    :param mqtt_packet: DeviceMQTTPacket
    :return: String con el paquete formateado
    """
    return f'\n Topic: {mqtt_packet.topic}. \n Message: \n {mqtt_packet.payload}'


class InvalidDeviceIdError(Exception):
    """
    Excepcion que indica que la id del dispositivo no es valida
    """
    def __init__(self, mqtt_packet):
        Exception.__init__(self, f'The DeviceId is not valid. {log_packet(mqtt_packet)}')


class InvalidTopicError(Exception):
    """
    Excepcion que indica que el topic no es valido
    """
    def __init__(self, mqtt_packet):
        Exception.__init__(self, f'The Topic is not valid. {log_packet(mqtt_packet)}')


class InvalidPayloadError(Exception):
    """
    Excepcion que indica que el mensaje no es valido
    """
    def __init__(self, mqtt_packet):
        Exception.__init__(self, f'The Payload is not valid. {log_packet(mqtt_packet)}')