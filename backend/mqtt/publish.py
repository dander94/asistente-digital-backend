import uuid

from mqtt.constants import MQTT_TOPIC_CONF_RESP, MQTT_BRIDGE_DELIMITER, MQTT_TOPIC_BRIDGE


def bridge_publish(topic, payload):
    """
    Metodo para publicar utilizando el metodo bridge. Vease on_bridge en run_mqtt_client.py
    :param topic:
    :param payload:
    :return:
    """
    from mqtt.client import init_mqtt

    b_payload = f'{topic}{MQTT_BRIDGE_DELIMITER}{payload}'
    c_id = str(uuid.uuid4())[:6]
    client = init_mqtt(client_id=c_id, clean_session=True)

    b_topic = f'{MQTT_TOPIC_BRIDGE}/{c_id}/'
    client.publish(b_topic, payload=b_payload)
    client.disconnect()


def mqtt_publish_conf(device, client=None):
    """
    Metodo que usa el sistema bridge para publicar la configuracion de un dispositivo.
    :param client:
    :param device:
    :return:
    """
    min_temperature = device.min_temperature if device.min_temperature is not None else 0
    max_temperature = device.max_temperature if device.max_temperature is not None else 0
    # Cambiamos el formato para que siempre sea dos digitos
    min_temperature = str(min_temperature).zfill(2)
    max_temperature = str(max_temperature).zfill(2)
    wifi_ssid = device.wifi_ssid or ''
    wifi_pass = device.wifi_password or ''
    len_wifi_ssid = str(len(wifi_ssid)).zfill(2)
    len_wifi_pass = str(len(wifi_pass)).zfill(2)
    payload = f'{int(device.is_night_mode_enabled)};{min_temperature};{max_temperature};' \
        f'{len_wifi_ssid};{len_wifi_pass};{wifi_ssid};{wifi_pass}'
    if client is None:
        bridge_publish(MQTT_TOPIC_CONF_RESP.format(device=device.identifier), payload)
    else:
        client.publish(MQTT_TOPIC_CONF_RESP.format(device=device.identifier), payload=payload)
