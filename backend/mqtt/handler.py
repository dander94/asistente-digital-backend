import json
import re

from django.utils import timezone

from device.models import Device, DeviceSyncCode
from device.utils import send_notification_for_device
from mqtt.constants import MQTT_DEVICE_ID_REGEX, MQTT_TOPIC_CONF_REQ, \
    MQTT_TOPIC_SYNC_REQ, MQTT_TOPIC_NOTIF, MQTT_TOPIC_HEARTB_REQ, MQTT_TOPIC_SYNC_RESP, MQTT_TOPIC_HEARTB_RESP
from mqtt.exceptions import InvalidDeviceIdError, InvalidTopicError, InvalidPayloadError
from mqtt.publish import mqtt_publish_conf

DEVICE_ID_REGEX = re.compile(MQTT_DEVICE_ID_REGEX)


class DeviceMQTTPacket:
    """
        Clase que contiene los atributos y la funcionalidad necesaria para gestionar un paquete recibido de MQTT
    """
    client = None
    device = None
    topic = None
    payload = None

    def __init__(self, client, topic, payload):
        """
        Funcion de inicializacion
        :param client: Cliente MQTT
        :param topic: Topic del mensaje
        :param payload: Payload del mensaje
        """
        # Guardamos los atributos de la clase
        self.client = client
        self.topic = f'{topic}/' if not topic[-1] == '/' else topic
        self.payload = payload.decode('utf-8') if payload else None

        # Comprobamos cual es la id del dispositivo segun su topic
        try:
            device_id = DEVICE_ID_REGEX.search(topic).group(1)
        except (IndexError, AttributeError):
            raise InvalidDeviceIdError(self)

        # Recuperamos o creamos el dispositivo
        self.device = Device.objects.get_or_create(
            identifier=device_id
        )[0]

    def _handle_sync_req(self):
        """
            Funcion a ejecutar cuando el paquete sea un sync rec
        """
        ret_payload = DeviceSyncCode.objects.get_or_create_for_device(self.device).sync_code

        self.client.publish(MQTT_TOPIC_SYNC_RESP.format(device=self.device.identifier), payload=ret_payload)

    def _handle_conf_req(self):
        """
            Funcion a ejecutar cuando el paquete sea un conf rec
        """
        mqtt_publish_conf(self.device, client=self.client)

    def _handle_notification(self):
        """
            Funcion a ejecutar cuando el paquete sea una notificacion
        """
        if not send_notification_for_device(self.device, self.payload):
            raise InvalidPayloadError(self)

    def _handle_heartbeat_req(self):
        """
            Funcion a ejecutar cuando el paquete sea un hearbeat
        """
        self.device.set_device_on()
        self.client.publish(MQTT_TOPIC_HEARTB_RESP.format(device=self.device.identifier), payload="")

    def do_action(self):
        """
            Funcion intemedia para gestionar el paquete, eligiendo cual es el handler para el tipo de este.
        :return:
        """
        handlers = {
            MQTT_TOPIC_CONF_REQ.format(device=self.device.identifier): self._handle_conf_req,
            MQTT_TOPIC_SYNC_REQ.format(device=self.device.identifier): self._handle_sync_req,
            MQTT_TOPIC_NOTIF.format(device=self.device.identifier): self._handle_notification,
            MQTT_TOPIC_HEARTB_REQ.format(device=self.device.identifier): self._handle_heartbeat_req
        }
        try:
            handlers[self.topic]()
        except KeyError:
            raise InvalidTopicError(self)

