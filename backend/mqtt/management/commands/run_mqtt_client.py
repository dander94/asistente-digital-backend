import traceback

from django.core.management import BaseCommand

from mqtt.client import init_mqtt
from mqtt.constants import MQTT_TOPIC_SYNC_TOPICS, MQTT_TOPIC_NOTIF_TOPICS, MQTT_TOPIC_HEARTB_TOPICS, \
    MQTT_TOPIC_CONF_TOPICS, MQTT_TOPIC_BRIDGE, MQTT_BRIDGE_DELIMITER, MQTT_TOPIC_BRIDGE_TOPICS
from mqtt.exceptions import InvalidDeviceIdError, InvalidTopicError, InvalidPayloadError
from mqtt.handler import DeviceMQTTPacket


def on_message(client, userdata, message):
    """
        Callback para cuando se recibe un mensaje de un topic suscrito
    :param client: Cliente MQTT
    :param userdata: Datos privados del usuario
    :param message: Mensaje recibido
    :return:
    """
    # Se crea un nuevo DeviceMQTTPacket con los datos y se realiza la acción pertinente al tipo de mensaje
    print(f'Package received. {message.topic}: {message.payload}')
    try:
        DeviceMQTTPacket(client, message.topic, message.payload).do_action()
    except (InvalidDeviceIdError, InvalidTopicError, InvalidPayloadError) as e:
        print("MQTT Exception: \n")
        traceback.print_exc()
    except Exception as e:
        print("Uncaught Exception: \n")
        traceback.print_exc()


def on_bridge(client, userdata, message):
    """
    El servidor Django será multihilo, por lo que mantener una única conexión de MQTT puede ser bastante complicado
    y un cuello de botella. Para evitarlo, cada hilo de django se conectará con una id de cliente random, y
    dejará un mensaje en bridge. Este lo recibirá nuestro cliente y lo enviará al topic correspondiente, de forma
    que se podrá mantener sin limpiar la sesión.
    :param client:
    :param userdata:
    :param message:
    :return:
    """
    topic, payload = message.payload.decode('utf-8').split(MQTT_BRIDGE_DELIMITER)
    client.publish(topic=topic, payload=payload)


def on_connect(client, userdata, flags, rc):
    """
    Callback para cuando el cliente se conecte al broker
    :param client: Cliente MQTT
    :param userdata: Datos privados del usuario
    :param flags: Flags enviadas por el broker
    :param rc: Resultado de la conexion.
    :return:
    """
    if rc == 0:
        # La conexion ha funcionado correctamente. Suscribimos al cliente a todos los topics necesarios.
        client.subscribe(MQTT_TOPIC_SYNC_TOPICS, qos=0)
        client.subscribe(MQTT_TOPIC_CONF_TOPICS, qos=0)
        client.subscribe(MQTT_TOPIC_NOTIF_TOPICS, qos=0)
        client.subscribe(MQTT_TOPIC_HEARTB_TOPICS, qos=0)
        client.subscribe(MQTT_TOPIC_BRIDGE_TOPICS, qos=0)
        # Anyadimos el callback para cuando se reciba un mensaje
        client.message_callback_add(MQTT_TOPIC_SYNC_TOPICS, on_message)
        client.message_callback_add(MQTT_TOPIC_CONF_TOPICS, on_message)
        client.message_callback_add(MQTT_TOPIC_NOTIF_TOPICS, on_message)
        client.message_callback_add(MQTT_TOPIC_HEARTB_TOPICS, on_message)
        client.message_callback_add(MQTT_TOPIC_BRIDGE_TOPICS, on_bridge)
    else:
        print(f'Couldnt connect to MQTT. Return code: {rc}')


class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            print("MQTT Client. Alejandro Diaz Perez")
            client = init_mqtt(client_id='server_client', on_connect=on_connect)
            client.loop_forever()
        except KeyboardInterrupt:
            print('\nBye!')
