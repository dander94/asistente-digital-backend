MQTT_PACKET_TYPE_SYNC = 'sync'  # Sincronizacion
MQTT_PACKET_TYPE_CONF = 'conf'  # Configuracion
MQTT_PACKET_TYPE_NOTIF = 'notf'  # Notificacion
MQTT_PACKET_TYPE_HEARTB = 'hrtb'  # Heartbeat

MQTT_TOPIC_SYNC_TOPICS = f'{MQTT_PACKET_TYPE_SYNC}/#'
MQTT_TOPIC_CONF_TOPICS = f'{MQTT_PACKET_TYPE_CONF}/#'
MQTT_TOPIC_NOTIF_TOPICS = f'{MQTT_PACKET_TYPE_NOTIF}/#'
MQTT_TOPIC_HEARTB_TOPICS = f'{MQTT_PACKET_TYPE_HEARTB}/#'
MQTT_TOPIC_SYNC_REQ = f'{MQTT_PACKET_TYPE_SYNC}/' + '{device}/'  # Peticion sync
MQTT_TOPIC_SYNC_RESP = '{device}/' + f'{MQTT_PACKET_TYPE_SYNC}'  # Respuesta SYNC
MQTT_TOPIC_CONF_REQ = f'{MQTT_PACKET_TYPE_CONF}/' + '{device}/'  # Peticion Conf
MQTT_TOPIC_CONF_RESP = '{device}/' + f'{MQTT_PACKET_TYPE_CONF}'  # Respuesta Conf
MQTT_TOPIC_NOTIF = f'{MQTT_PACKET_TYPE_NOTIF}/' + '{device}/'  # Notificacion
MQTT_TOPIC_HEARTB_REQ = f'{MQTT_PACKET_TYPE_HEARTB}/' + '{device}/'  # Heartbeat
MQTT_TOPIC_HEARTB_RESP = '{device}/' + f'{MQTT_PACKET_TYPE_HEARTB}'  # Respuesta heartbeat

# Regex para saber el tipo de paquete
MQTT_DEVICE_ID_REGEX = f'(?:{MQTT_PACKET_TYPE_SYNC}|' + \
                            f'{MQTT_PACKET_TYPE_CONF}|{MQTT_PACKET_TYPE_NOTIF}|{MQTT_PACKET_TYPE_HEARTB})/' + \
                            '([^/]+)/?$'

# Este topic se usa para que las publicaciones hacia los dispositivos sean constantes.
MQTT_TOPIC_BRIDGE = 'bridge'
MQTT_TOPIC_BRIDGE_TOPICS = f'{MQTT_TOPIC_BRIDGE}/#'
MQTT_BRIDGE_DELIMITER = '<!>'