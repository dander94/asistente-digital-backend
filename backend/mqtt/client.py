import ssl

from paho.mqtt import client as paho_client

from django.conf import settings


def init_mqtt(client_id=None, clean_session=False, on_connect=None):
    """
    Funcion para inicializar el cliente MQTT
    :return:
    """
    # Creamos el cliente con la id de django_server
    mqtt_client = paho_client.Client(client_id=client_id, clean_session=clean_session)
    # Definimos el callback para la conexion
    mqtt_client.on_connect = on_connect
    # Definimos el nombre de usuario y la contrasenya para conectar con el broker
    mqtt_client.username_pw_set(settings.MQTT_USER, settings.MQTT_PASSWORD)
    # Procedemos a realizar la conexion con los valores por defecto (keepalive de 60 segundos)
    try:
        mqtt_client.tls_set(settings.MQTT_CERT_FILE, tls_version=ssl.PROTOCOL_TLSv1_2)
        mqtt_client.tls_insecure_set(True)  # No se verificara el certificado de momento
        mqtt_client.connect(settings.MQTT_SERVER, settings.MQTT_PORT)
    except ConnectionRefusedError:
        print(f'Couldnt connect to MQTT. Connection Refused')
        return None
    return mqtt_client
