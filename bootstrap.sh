#!/usr/bin/env bash
add-apt-repository -y ppa:deadsnakes/ppa
apt-get update
apt-get -y install python3.8

update-alternatives  --set python /usr/bin/python3.8

# Python 3.8 and typical libraries
apt-get -y install python3.8-dev
apt-get -y install libpq-dev
apt-get -y install libffi-dev libssl-dev libxml2-dev libxslt1-dev
apt-get -y install libxmlsec1 libxmlsec1-dev swig

apt-get -y install git

apt-get -y install python3-pip

alias python=python3.8

# Postgresql 12
echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
apt-get update
apt-get -y install postgresql-12 pgadmin4

service postgresql restart

sudo -u postgres bash -c "psql -c \"CREATE USER django WITH PASSWORD 'django';\""
sudo -u postgres createdb --owner=django --encoding=UTF8 django_db

# Mosquitto
apt-get -y install mosquitto
touch /etc/mosquitto/passwd
mosquitto_passwd -b /etc/mosquitto/passwd msp_notifier "4]6<u,yMQ{fa^rht}lsJuS+B<[2U0N"
rm /etc/mosquitto/mosquitto.conf
cp /etc/mosquitto/conf.d/default.conf /etc/mosquitto/mosquitto.conf
cp /home/vagrant/backend/certs/ca.crt  /etc/mosquitto/certs/.
cp /home/vagrant/backend/certs/server.crt  /etc/mosquitto/certs/.
cp /home/vagrant/backend/certs/server.key  /etc/mosquitto/certs/.
echo "bind_address 0.0.0.0" >> /etc/mosquitto/mosquitto.conf
echo "allow_anonymous false" >> /etc/mosquitto/mosquitto.conf
echo "password_file /etc/mosquitto/passwd" >> /etc/mosquitto/mosquitto.conf
echo "port 8883" >> /etc/mosquitto/mosquitto.conf
echo "cafile /etc/mosquitto/certs/ca.crt" >> /etc/mosquitto/mosquitto.conf
echo "certfile /etc/mosquitto/certs/server.crt" >> /etc/mosquitto/mosquitto.conf
echo "keyfile /etc/mosquitto/certs/server.key" >> /etc/mosquitto/mosquitto.conf
echo "tls_version tlsv1_2" >> /etc/mosquitto/mosquitto.conf
service mosquitto restart

# Virtualenv
pip3 install --upgrade pip
pip3 install virtualenv
pip3 install virtualenvwrapper

echo "export WORKON_HOME=~/Envs" >> /home/vagrant/.profile
echo "export VIRTUALENVWRAPPER_PYTHON='/usr/bin/python3'" >> /home/vagrant/.profile
echo "source /usr/local/bin/virtualenvwrapper.sh" >> /home/vagrant/.profile
echo "workon env" >> /home/vagrant/.profile
echo "cd /home/vagrant/backend" >> /home/vagrant/.profile
echo "alias python=python3.8" >> /home/vagrant/.profile

su - vagrant << EOF
export WORKON_HOME=~/Envs
source /usr/local/bin/virtualenvwrapper.sh
mkvirtualenv --python=/usr/bin/python3.8 env
workon env
cd /home/vagrant/backend/
pip install -r requirements.txt
EOF

