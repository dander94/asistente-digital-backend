# Backend Asistente Digital

Este proyecto está hecho con Vagrant, Django y Mosquitto como tecnologías principales.


## Setup
Para instalarlo, debe instalar [VirtualBox](https://www.virtualbox.org/) y [Vagrant](https://www.vagrantup.com/). 

Después, en el directorio principal, debe realizar:
`vagrant up`

Una vez finalice, podrá acceder a la máquina con:
`vagrant ssh`

Dentro de esta, deberá migrar la base de datos. Para ello realice
`cd backend`
`python manage.py migrate`

Con esto el proyecto esta listo para ser utilizado.


## Arrancar el servidor
Logueese en la máquina con
`vagrant ssh`

Ejecute
`cd backend`
`python manage.py runserver 0.0.0.0:8000`

Con esto, podrá ver el sitio web en la dirección localhost:8002

## Arrancar el cliente MQTT
Logueese en la máquina con
`vagrant ssh`

Ejecute
`cd backend`
`python manage.py run_mqtt_client`

Con esto, el cliente MQTT del servidor estara arrancado y responderá a las peticiones del dispostivo.

## Arrancar el Comprobador de Dispositivos
 Logueese en la máquina con
`vagrant ssh`

Ejecute
`cd backend`
`python manage.py check_devices_on`

Con esto, el servidor comprobará si los dispositivos que estaban conectados se han apagado, 
y se enviará por lo tanto una notificación.

## Broker MQTT
El broker MQTT se inicia automaticamente con la máquina.

El usuario y la contraseña del broker mqtt son: `msp_notifier / 4]6<u,yMQ{fa^rht}lsJuS+B<[2U0N`


## Probando MQTT
Si desea probar manualmente MQTT se recomienda la herramienta [MQTT Explorer](http://mqtt-explorer.com/)

De ahora en adelante, <device_id> deberá ser reemplazado por una id para su dispositivo. Se aconseja que contenga 8 caracteres alfanuméricos.

### Sync Req / Resp
Publique un paquete vació en el topic sync/<device_id>

El servidor responderá con un sync code en el topic
<device_id>/sync

### Conf
Para que el servidor publique la configuracion bien puede:
- Modificar la configuracion de su dispositivo
- Hacer una petición.

En ambos casos se publicará en el topic: <device_id>/conf

Para hacer una peticion, publique un paquete vacio en el topic conf/<device_id>

### Notificacion
Para que el servidor envie una notificacion será haya registrado en el sitio web, haya vinculado su dispositivo (utilizando su <device_id> inventada), y haya activado las notificaciones.

Publique un mensaje en el topic notf/<device_id>.

El contenido debe ser:
Notificacion Arriba:
`up`
Notificacion Izquierda
`le`
Notificacion Abajo
`do`
Notificacion Derecha
`ri`
Notificacion Arriba Izquierda
`ul`
Notificacion Arriba Derecha
`ur`

## Archivos

### .env
Fichero que contiene las variables de entorno.

### bootstrap.sh
Fichero sh ejecutado por vagrant para provisionar la máquina. Instala todas las depencias del proyecto.

### requirements.txt
Contiene las librerias de Python que pip debe instalar.

### Vagrantfile
Contiene la configuración de la máquina Vagrant. (Imagen de sistema, puertos...)

### Carpeta Backend:
Contiene el proyecto de Django. 

Sus apps son las siguientes:

Backend:
- Carpeta que contiene la configuración y las urls del sistema.
- Ficheros interesantes: 
    - settings.py
    - urls.py

Base:
- Carpeta que contiene la template básica y un mixin de login.
- Ficheros interesantes: 
    - templates/base/base.html
    - mixins.py
  
Device:
- Contiene la funcionalidad y los modelos del dispositivo. Así como las vistas principales
del sistema. Se recomienda mirar todos los ficheros excepto admin.py y apps.py.
En management/commands/check_devices_on.py encontrá el script para buscar dispositivos apagados.

MQTT:
- Contiene la funcionalidad MQTT del sistema para publicar y el cliente intermedio de subscripción.
- Ficheros interesantes:
    - client.py
    - constants.py
    - exceptions.py
    - handler.py
    - publish.py
    - management/commands/run_mqtt_client.py <- Este el cliente intermedio.
    
User:
- Contiene la funcionalidad de usuarios del sistema. Se recomienda mirar todos los ficheros excepto apps.py y admin.py